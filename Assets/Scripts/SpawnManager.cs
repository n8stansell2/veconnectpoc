﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class SpawnManager : MonoBehaviour
{
    private GameManager gm;
    private int index = 0;
    public GameObject[] loadedProducts;
    private List<GameObject> newObjects;
    public List<int> originalIDs;
    public bool LoadedLocal = false;
    private string loadTitle = string.Empty;
    private string loadText = string.Empty;
    private string percentDone = "0.00 %";
    private float sliderVal = 0;
    public GameObject LoadingPanel;
    public Slider LoadingSlider;
    public TextMeshProUGUI LoadingTitle;
    public TextMeshProUGUI LoadingText;
    public TextMeshProUGUI LoadingPercentText;
    
    // Start is called before the first frame update
    void Start()
    {
        newObjects = new List<GameObject>();
        originalIDs = new List<int>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnProduct(float offset)
    {
        if(LoadedLocal)
        {
            LoadingPanel.SetActive(false);
            LoadingSlider.value = 0;
            LoadingPercentText.SetText("0.0 %");
            LoadingText.SetText(string.Empty);

            GameObject go = loadedProducts[index];

            Vector3 spawnPos = new Vector3(gm.SpawnPosX, gm.SpawnPosY, gm.SpawnPosZ);
            DownwardRotation dr = go.GetComponent<DownwardRotation>();
            dr.Offset = offset;
            dr.MovementEnabled = true;

            ProductData pd = go.GetComponent<ProductData>();

            go.SetActive(true);

            Instantiate(go, spawnPos, go.transform.rotation);

            GameObject price = Instantiate(gm.ProductPrice, spawnPos, gm.ProductPrice.transform.rotation);
            DownwardRotation priceDr = price.GetComponent<DownwardRotation>();
            priceDr.Offset = offset;
            priceDr.MovementEnabled = true;
            GameObject priceTxt = price.gameObject.transform.GetChild(0).gameObject;
            priceTxt.GetComponent<TextMeshPro>().SetText(pd.Price);

            GameObject oos = Instantiate(gm.OutOfStock, spawnPos, gm.OutOfStock.transform.rotation);
            DownwardRotation oosDr = oos.GetComponent<DownwardRotation>();
            oosDr.Offset = offset;
            oosDr.MovementEnabled = true;

            oos.SetActive(!pd.Available);

            index++;

            if (index >= loadedProducts.Length)
            {
                index = 0;
            }
        }
        else
        {
            ProductStuff ps = gm.LoadedProducts[index];

            GameObject go = SpawnFromFile(ps.ProductName, ps.OBJFile, ps.TextureImage);
            ProductData pd = go.AddComponent<ProductData>();
            pd.ID = ps.OBJGUID;
            pd.ProductID = ps.ProductID;
            pd.NutritionalImage = ps.NutritionalImage;
            pd.Price = ps.Price;
            pd.Available = true;
            go.tag = "IsProduct";
            go.SetActive(false);
            go.transform.position = new Vector3(gm.SpawnPosX, gm.SpawnPosY, gm.SpawnPosZ);
            go.GetComponent<DownwardRotation>().Offset = offset;
            originalIDs.Add(go.GetInstanceID());
            newObjects.Add(go);

            index++;

            loadTitle = "Loading Objects Into Scene . . .";
            loadText = $"Loading {ps.ProductName} - {ps.OBJFile.Length} bytes";
            sliderVal = ((float)index / gm.LoadedProducts.Length);
            percentDone = string.Format("{0:0.0} %", ((decimal)index / gm.LoadedProducts.Length) * 100);

            ShowLoadingPanel(sliderVal, loadTitle, percentDone, loadText);
            
            if (index >= gm.LoadedProducts.Length)
            {
                index = 0;

                if (!LoadedLocal)
                {
                    this.loadedProducts = newObjects.ToArray();
                    LoadedLocal = true;
                }
            }
        }
    }

    public void LoadNewObjects()
    {
        newObjects.Clear();
        loadedProducts = null;
        LoadedLocal = false;
    }

    public void ShowLoadingPanel(float barValue, string title, string percent, string loadText)
    {
        LoadingPanel.SetActive(true);
        LoadingSlider.value = barValue;
        LoadingTitle.SetText(title);
        LoadingPercentText.SetText(percent);
        LoadingText.SetText(loadText);
    }

    public void HideLoadingPanel()
    {
        LoadingPanel.SetActive(false);
        LoadingSlider.value = 0;
        LoadingPercentText.SetText("0.0 %");
        LoadingText.SetText(string.Empty);
        LoadingTitle.SetText(string.Empty);
    }

    public void ResetObjects()
    {
        GameObject[] objs = FindObjectsOfType<GameObject>() as GameObject[];

        foreach (GameObject go in objs)
        {
            Debug.Log($"Found {objs.Length} objects");

            if (!originalIDs.Contains(go.GetInstanceID()))
            {
                if(go.CompareTag("IsProduct") || go.CompareTag("IsProductPrice"))
                {
                    Destroy(go);
                }
            }
        }

        Debug.Log($"Remaining viable products {this.loadedProducts.Length}");
    }

    public int VisibleObjects()
    {
        GameObject[] objs = FindObjectsOfType<GameObject>() as GameObject[];
        int visible = objs.Where(x => x.activeInHierarchy && x.CompareTag("IsProduct")).Count();
        Debug.Log($"{visible} objects visible");
        return visible;
    }

    public GameObject SpawnFromFile(string productName, byte[] objData, byte[] textureData)
    {
        Debug.Log($"Importing {productName}");
        Mesh mesh = ObjImporter.ImportFile(productName, objData);
        Texture2D tx = new Texture2D(4, 4, TextureFormat.RGBA32, false);
        tx.LoadImage(textureData);

        GameObject go = new GameObject();
        MeshFilter mf = go.AddComponent<MeshFilter>();
        MeshRenderer mr = go.AddComponent<MeshRenderer>();
        mr.material.mainTexture = tx;
        mf.mesh = mesh;

        Vector3 scale = go.transform.localScale;
        go.transform.localScale = new Vector3(-scale.x, scale.y, scale.z);

        go.AddComponent<BoxCollider>();
        go.AddComponent<DownwardRotation>();
        go.name = productName;

        ConvertMeshToUnityScale(go, 1.5f);

        return go;
    }

    public void ConvertMeshToUnityScale(GameObject theGameObject, float newSize, string axis = "x")
    {
        Renderer renderer = theGameObject.GetComponent<Renderer>();
        float size = renderer.bounds.size.x;

        /*if (axis.ToLower() == "x")
        {
            size = renderer.bounds.size.x;
        }
        else if (axis.ToLower() == "z")
        {
            size = renderer.bounds.size.z;
        }*/

        if(renderer.bounds.size.x > renderer.bounds.size.y)
        {
            size = renderer.bounds.size.y;
        }

        Vector3 rescale = theGameObject.transform.localScale;

        rescale.x = newSize * rescale.x / size;
        rescale.y = newSize * rescale.y / size;
        rescale.z = newSize * rescale.z / size;

        theGameObject.transform.localScale = rescale;
    }
}

public static class ObjImporter
{
    public static Mesh ImportFile(string productName, byte[] data)
    {
        meshStruct newMesh = createMeshStruct(productName, data);
        populateMeshStruct(ref newMesh, data);

        Vector3[] newVerts = new Vector3[newMesh.faceData.Length];
        Vector2[] newUVs = new Vector2[newMesh.faceData.Length];
        Vector3[] newNormals = new Vector3[newMesh.faceData.Length];
        int i = 0;
        
        //The following foreach loops through the facedata and assigns the appropriate vertex, uv, or normal
        //for the appropriate Unity mesh array.

        foreach (Vector3 v in newMesh.faceData)
        {
            newVerts[i] = newMesh.vertices[(int)v.x - 1];
            if (v.y >= 1)
            {
                newUVs[i] = newMesh.uv[(int)v.y - 1];
            }

            if (v.z >= 1)
            {
                newNormals[i] = newMesh.normals[(int)v.z - 1];
            }
                
            i++;
        }

        Mesh mesh = new Mesh();

        mesh.vertices = newVerts;
        mesh.uv = newUVs;
        mesh.normals = newNormals;
        mesh.triangles = newMesh.triangles;

        mesh.RecalculateBounds();
        //mesh.Optimize();

        return mesh;
    }

    private static meshStruct createMeshStruct(string productName, byte[] data)
    {
        int triangles = 0;
        int vertices = 0;
        int vt = 0;
        int vn = 0;
        int face = 0;
        meshStruct mesh = new meshStruct();
        mesh.fileName = productName;
        string entireText = Encoding.UTF8.GetString(data);

        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();
            char[] splitIdentifier = { ' ' };
            string[] brokenString;
            while (currentText != null)
            {
                if (!currentText.StartsWith("f ") && !currentText.StartsWith("v ") && !currentText.StartsWith("vt ") && !currentText.StartsWith("vn "))
                {
                    currentText = reader.ReadLine();

                    if (currentText != null)
                    {
                        currentText = currentText.Replace("  ", " ");
                    }
                }
                else
                {
                    currentText = currentText.Trim();                           //Trim the current line
                    brokenString = currentText.Split(splitIdentifier, 50);      //Split the line into an array, separating the original line by blank spaces

                    switch (brokenString[0])
                    {
                        case "v":
                            vertices++;
                            break;
                        case "vt":
                            vt++;
                            break;
                        case "vn":
                            vn++;
                            break;
                        case "f":
                            face = face + brokenString.Length - 1;
                            triangles = triangles + 3 * (brokenString.Length - 2); /*brokenString.Length is 3 or greater since a face must have at least
                                                                                     3 vertices.  For each additional vertice, there is an additional
                                                                                     triangle in the mesh (hence this formula).*/
                            break;
                    }

                    currentText = reader.ReadLine();

                    if (currentText != null)
                    {
                        currentText = currentText.Replace("  ", " ");
                    }
                }
            }
        }

        mesh.triangles = new int[triangles];
        mesh.vertices = new Vector3[vertices];
        mesh.uv = new Vector2[vt];
        mesh.normals = new Vector3[vn];
        mesh.faceData = new Vector3[face];

        return mesh;
    }

    private static void populateMeshStruct(ref meshStruct mesh, byte[] data)
    {
        string entireText = Encoding.UTF8.GetString(data);

        using (StringReader reader = new StringReader(entireText))
        {
            string currentText = reader.ReadLine();

            char[] splitIdentifier = { ' ' };
            char[] splitIdentifier2 = { '/' };
            string[] brokenString;
            string[] brokenBrokenString;
            int f = 0;
            int f2 = 0;
            int v = 0;
            int vn = 0;
            int vt = 0;
            int vt1 = 0;
            int vt2 = 0;

            while (currentText != null)
            {
                if (!currentText.StartsWith("f ") && !currentText.StartsWith("v ") && !currentText.StartsWith("vt ") &&
                    !currentText.StartsWith("vn ") && !currentText.StartsWith("g ") && !currentText.StartsWith("usemtl ") &&
                    !currentText.StartsWith("mtllib ") && !currentText.StartsWith("vt1 ") && !currentText.StartsWith("vt2 ") &&
                    !currentText.StartsWith("vc ") && !currentText.StartsWith("usemap "))
                {
                    currentText = reader.ReadLine();
                    if (currentText != null)
                    {
                        currentText = currentText.Replace("  ", " ");
                    }
                }
                else
                {
                    currentText = currentText.Trim();
                    brokenString = currentText.Split(splitIdentifier, 50);

                    switch (brokenString[0])
                    {
                        case "g":
                            break;
                        case "usemtl":
                            break;
                        case "usemap":
                            break;
                        case "mtllib":
                            break;
                        case "v":
                            mesh.vertices[v] = new Vector3(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]), System.Convert.ToSingle(brokenString[3]));
                            v++;
                            break;
                        case "vt":
                            mesh.uv[vt] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                            vt++;
                            break;
                        case "vt1":
                            mesh.uv[vt1] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                            vt1++;
                            break;
                        case "vt2":
                            mesh.uv[vt2] = new Vector2(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]));
                            vt2++;
                            break;
                        case "vn":
                            mesh.normals[vn] = new Vector3(System.Convert.ToSingle(brokenString[1]), System.Convert.ToSingle(brokenString[2]), System.Convert.ToSingle(brokenString[3]));
                            vn++;
                            break;
                        case "vc":
                            break;
                        case "f":

                            int j = 1;
                            List<int> intArray = new List<int>();

                            while (j < brokenString.Length && ("" + brokenString[j]).Length > 0)
                            {
                                Vector3 temp = new Vector3();
                                brokenBrokenString = brokenString[j].Split(splitIdentifier2, 3);    //Separate the face into individual components (vert, uv, normal)
                                temp.x = System.Convert.ToInt32(brokenBrokenString[0]);

                                if (brokenBrokenString.Length > 1)                                  //Some .obj files skip UV and normal
                                {
                                    if (brokenBrokenString[1] != "")                                    //Some .obj files skip the uv and not the normal
                                    {
                                        temp.y = System.Convert.ToInt32(brokenBrokenString[1]);
                                    }

                                    if(brokenBrokenString.Length > 2)
                                    {
                                        temp.z = System.Convert.ToInt32(brokenBrokenString[2]);
                                    }
                                    else
                                    {
                                        temp.z = temp.x;
                                    }
                                }

                                j++;

                                mesh.faceData[f2] = temp;
                                intArray.Add(f2);
                                f2++;
                            }

                            j = 1;

                            while (j + 2 < brokenString.Length)     //Create triangles out of the face data.  There will generally be more than 1 triangle per face.
                            {
                                mesh.triangles[f] = intArray[0];
                                f++;
                                mesh.triangles[f] = intArray[j];
                                f++;
                                mesh.triangles[f] = intArray[j + 1];
                                f++;

                                j++;
                            }
                            break;
                    }

                    currentText = reader.ReadLine();

                    if (currentText != null)
                    {
                        currentText = currentText.Replace("  ", " ");       //Some .obj files insert double spaces, this removes them.
                    }
                }
            }
        }
    }
}

public struct meshStruct
{
    public Vector3[] vertices;
    public Vector3[] normals;
    public Vector2[] uv;
    public Vector2[] uv1;
    public Vector2[] uv2;
    public int[] triangles;
    public int[] faceVerts;
    public int[] faceUVs;
    public Vector3[] faceData;
    public string name;
    public string fileName;
}