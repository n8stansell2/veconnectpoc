﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewProductInfo : MonoBehaviour
{
    private Button button;
    private GameManager gm;
    
    // Start is called before the first frame update
    void Start()
    {
        button = this.GetComponent<Button>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        button.onClick.AddListener(ShowProductInfo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ShowProductInfo()
    {
        if(button.CompareTag("ViewProductInfo"))
        {
            gm.SetNutritionalImage();
            gm.ViewProductInfoPlane.SetActive(true);
        }
        else if(button.CompareTag("ViewProduct"))
        {
            gm.ViewProductInfoPlane.SetActive(false);
        }
        else if(button.CompareTag("Exit"))
        {
            gm.ReturnProductToSpiral();
        }
        else if(button.CompareTag("Purchase"))
        {
            gm.SellCurrentProduct();
        }
    }
}
