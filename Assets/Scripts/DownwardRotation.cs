﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class DownwardRotation : MonoBehaviour
{
    private GameManager gm;
    private SpawnManager spwnmgr;
    private ProductData productInfo;
    private float timeCounter = 0;
    private float spiralWidth = 0;
    private float currentTC = 0;
    private float mouseX;
    private float mouseY;
    [SerializeField] float angle;
    [SerializeField] float angleCalc;
    private bool viewProduct = false;
    public float Offset;
    [SerializeField] Vector3 targetDestination;
    private Vector3 gotoDestination;
    private Quaternion gotoRotation;
    [SerializeField] Vector3 currentPos;
    [SerializeField] Vector3 returnDestination;
    [SerializeField] Quaternion returnRotation;
    [SerializeField] Quaternion targetRotation;
    [SerializeField] float boost;
    [SerializeField] float posTotal;
    [SerializeField] float positionSpeed = 1;
    public bool MovementEnabled = false;
    private Vector3 difference;
    private bool moveProduct = false;
    private bool mouseDragging = false;
    private float posSpeed;
    private bool viewingProduct = false;
    private bool flyObject = false;
    private bool forwardSpin = false;
    private bool returningToSpiral = false;
    private float zRotation = 0.0f;
    private decimal price = 0.00m;
    private decimal credit = 0.00m;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        spwnmgr = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        productInfo = this.gameObject.GetComponent<ProductData>();

        gotoDestination = gm.GotoDestination;
        targetDestination = gotoDestination;
        gotoRotation = Quaternion.Euler(0, 0, 0);
        targetRotation = gotoRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if(MovementEnabled)
        {
            if (moveProduct)
            {
                if (posTotal > 0.01f)
                {
                    if (forwardSpin)
                    {
                        gm.TimeMaster += posSpeed;
                    }
                    else
                    {
                        gm.TimeMaster -= posSpeed;
                    }

                    posSpeed = getRotationPosition();
                }
                else
                {
                    moveProduct = false;
                    forwardSpin = false;
                    flyObject = true;

                    if (!viewingProduct)
                    {
                        returnDestination = this.gameObject.transform.position;
                        returnRotation = this.gameObject.transform.rotation;
                        targetDestination = gotoDestination;
                        targetRotation = gotoRotation;
                    }
                }
            }

            if (flyObject)
            {
                if (targetDestination != this.gameObject.transform.position || targetRotation != this.gameObject.transform.rotation)
                {
                    ChangePosition();
                }
                else
                {
                    if (!viewingProduct && !returningToSpiral)
                    {
                        this.gameObject.transform.position = gotoDestination;
                        this.gameObject.transform.rotation = gotoRotation;
                        viewingProduct = true;
                        gm.ProductHold = true;
                        gm.ProductViewPrice.SetText(this.productInfo.Price);

                        try
                        {
                            this.price = decimal.Parse(this.productInfo.Price, NumberStyles.AllowCurrencySymbol | NumberStyles.AllowDecimalPoint);
                        }
                        catch
                        {
                            this.price = 0.00m;
                        }
                        SetUiObjects();
                    }
                    else
                    {
                        viewingProduct = false;
                        mouseDragging = false;
                        gm.ProductHold = false;
                        gm.CurrentProductInstance = 0;

                        SetUiObjects(false);

                        gm.EnableSpin();
                        CheckObjectCount();
                    }

                    flyObject = false;
                    returningToSpiral = false;
                }
            }

            if (!flyObject && !gm.ProductHold)
            {
                HandleSpiraling();
            }

            if (gm.ResetProduct)
            {
                ReturnToTheSpiral();
                gm.ResetProduct = false;
            }
        }

        if(viewingProduct)
        {
            if(price > 0)
            {
                try
                {
                    credit = decimal.Parse(gm.CurrentUCBCredit, NumberStyles.AllowCurrencySymbol | NumberStyles.AllowDecimalPoint);
                    if(credit >= price)
                    {
                        gm.BuyNowButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        gm.BuyNowButton.gameObject.SetActive(false);
                    }
                }
                catch { }
            }
        }
    }

    private void SetUiObjects(bool show = true)
    {
        gm.ViewProduct.SetActive(show);
        gm.UCBCreditDisplay.SetActive(!show);
        gm.ViewProductInfoPlane.SetActive(false);
    }

    void HandleSpiraling()
    {
        timeCounter = gm.TimeMaster - Offset;
        spiralWidth = this.CompareTag("IsOutOfStock") ? gm.Width + 1 : gm.Width;

        float x = Mathf.Cos(timeCounter) * spiralWidth;
        float y = gm.SpawnPosY - timeCounter;
        float z = Mathf.Sin(timeCounter) * spiralWidth;

        transform.position = new Vector3(x, y, z);

        if(!this.CompareTag("IsProductPrice"))
        {
            Renderer rd = this.GetComponent<Renderer>();

            if (rd.bounds.size.x > rd.bounds.size.y)
            {
                zRotation = 90.0f;
            }

            angle = Mathf.Atan2(transform.position.x, transform.position.z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, angle, zRotation);

            targetRotation = this.transform.rotation;
            angleCalc = angle < 0 ? 180.0f : angle;
        }

        if(zRotation > 1.0f)
        {
            posTotal = Mathf.Abs(transform.rotation.x + transform.rotation.y);
        }
        else
        {
            posTotal = Mathf.Abs(transform.rotation.x + transform.rotation.y + transform.rotation.z);
        }

        if (transform.position.y < gm.ObjectFloor || transform.position.y > gm.ObjectCeiling)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }

        if (transform.position.y < -gm.ObjectDestroy)
        {
            if (!spwnmgr.originalIDs.Contains(this.GetInstanceID()))
            {
                Destroy(this.gameObject);
            }
        }
        else if(transform.position.y > gm.ObjectDestroy)
        {
            gm.ResetEnvironment = true;
        }
    }

    private void OnMouseDown()
    {
        if (gm.ProductHold && this.gameObject.GetInstanceID() == gm.CurrentProductInstance)
        {
            HandleMouseDown();
        }
        else if (gm.CurrentProductInstance == 0)
        {
            HandleMouseDown();
        }
    }

    private void HandleMouseDown()
    {
        StartCoroutine(ViewProduct());
        viewProduct = true;

        if (!viewingProduct)
        {
            mouseX = Input.mousePosition.x;
            mouseY = Input.mousePosition.y;
            currentTC = gm.TimeMaster;
        }
        else
        {
            if (!mouseDragging)
            {
                mouseX = Input.mousePosition.x;
                mouseY = Input.mousePosition.y;
            }
        }
    }

    private void OnMouseDrag()
    {
        if (gm.ProductHold && this.gameObject.GetInstanceID() == gm.CurrentProductInstance)
        {
            HandleMouseDrag();
        }
        else if (gm.CurrentProductInstance == 0)
        {
            HandleMouseDrag();
        }
    }

    private void HandleMouseDrag()
    {
        if (viewingProduct)
        {
            mouseDragging = true;
            this.gameObject.transform.rotation = new Quaternion((mouseY - Input.mousePosition.y) / 100, (mouseX - Input.mousePosition.x) / 100, 0, 1);
        }
        else
        {
            gm.TimeMaster = currentTC - ((mouseX - Input.mousePosition.x) / 100);
        }
    }

    private void OnMouseUp()
    {
        if(gm.ProductHold && this.gameObject.GetInstanceID() == gm.CurrentProductInstance)
        {
            HandleMouseUp();
        }
        else if (gm.CurrentProductInstance == 0)
        {
            HandleMouseUp();
        }
    }

    private void HandleMouseUp()
    {
        if (!gm.SaleInProgress)
        {
            if (viewProduct)
            {
                gm.CurrentProductID = this.gameObject.GetComponent<ProductData>().ProductID;
                gm.CurrentDataFileID = this.gameObject.GetComponent<ProductData>().ID;
                gm.CurrentProductInstance = this.gameObject.GetInstanceID();

                if (!viewingProduct)
                {
                    posSpeed = getRotationPosition();
                }

                gm.DisableSpin();

                if (viewingProduct)
                {
                    targetDestination = returnDestination;
                    targetRotation = returnRotation;
                    flyObject = true;
                    mouseX = Input.mousePosition.x;
                    mouseY = Input.mousePosition.y;
                    SetUiObjects(false);
                }
                else
                {
                    moveProduct = true;
                }
            }
            else
            {
                if (viewingProduct)
                {
                    viewingProduct = false;
                    flyObject = true;
                }
                else
                {
                    gm.EnableSpin();
                    CheckObjectCount();
                }
            }
        }

        viewProduct = false;
    }

    private void CheckObjectCount()
    {
        if(spwnmgr.VisibleObjects() < 115)
        {
            gm.ResetObjects();
        }
    }

    public void ReturnToTheSpiral()
    {
        targetDestination = returnDestination;
        targetRotation = returnRotation;
        flyObject = true;
        returningToSpiral = true;
        mouseX = Input.mousePosition.x;
        mouseY = Input.mousePosition.y;
        SetUiObjects(false);
    }

    private float getRotationPosition()
    {
        float result;

        result = (positionSpeed + (Mathf.Abs(transform.position.y - gm.ObjectStop))) / 100;

        if (transform.position.y > gm.ObjectStop)
        {
            forwardSpin = true;
        }

        if(result < 0.001f)
        {
            result = 0.001f;
        }

        return result;
    }

    IEnumerator ViewProduct()
    {
        yield return new WaitForSeconds(0.25f);
        viewProduct = false;
    }

    private void ChangePosition()
    {
        currentPos = this.gameObject.transform.position;
        difference = currentPos - targetDestination;
        boost = Mathf.Abs((difference.x + difference.y + difference.z) / 100);
        float delta = (gm.FloatSpeed * Time.deltaTime) + boost;

        Vector3 targetPos = Vector3.MoveTowards(currentPos, targetDestination, delta);

        this.gameObject.transform.position = targetPos;

        this.transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, gm.RotateSpeed);
    }
}
