﻿using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public ProductStuff[] LoadedProducts;
    private SpawnManager spwnmgr;
    public GameObject ViewProductInfoPlane;
    public GameObject ViewProduct;
    public TextMeshProUGUI ProductViewPrice;
    public GameObject ProductPrice;
    public GameObject OutOfStock;
    public TextMeshProUGUI ProductViewCredit;
    public TextMeshProUGUI ProductCredit;
    public GameObject UCBCreditDisplay;
    public TextMeshProUGUI UCBCredit;
    public GameObject StartupPanel;
    public TextMeshProUGUI StartupTitle;
    public TextMeshProUGUI StartupText;
    public Material NutriImageMaterial;
    public Button viewInfoButton;
    public Button viewProductButton;
    public Button BuyNowButton;
    public Button exitButton;
    public string CurrentUCBCredit = "$---";
    public decimal CurrentUCBCreditValue = 0.00m;
    public float SpawnPosX = 12.0f;
    public float SpawnPosY = 20.0f;
    public float SpawnPosZ = 12.0f;
    public float SpawnInterval = 1;
    public float ObjectCeiling = 32.0f;
    public float ObjectFloor = -32.0f;
    public float ObjectDestroy = -150.0f;
    public float ObjectStop = 15.9f;
    public float Speed = 3.0f;
    public float NormalSpeed = 0.1f;
    public float HighSpeed = 10.0f;
    public float Width = 12.0f;
    public float TextWidth = 12.0f;
    public float TimeMaster = 0;
    public float MasterOffset = 0;
    public float SpawnedProducts;
    public float InitialSpawnCount = 50;
    public float FloatSpeed = 1.0f;
    public float RotateSpeed = 5.0f;
    public string CurrentProductID = string.Empty;
    public string CurrentDataFileID = string.Empty;
    public int CurrentProductInstance = 0;
    public Vector3 GotoDestination = new Vector3(0, 14, 16);    
    public bool EnableSpiral = false;
    public bool EnableSpawnning = false;
    public bool ResetEnvironment = false;
    public bool ProductHold = false;
    public bool ResetProduct = false;
    private float objectCount = 0;
    private HubConnection hubConnection;
    private IHubProxy hubProxy;
    private string url = string.Empty;
    private List<string> productsList;
    public bool SaleInProgress = false;

    // Start is called before the first frame update
    void Start()
    {
        spwnmgr = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        this.EnableSpawnning = false;
        this.StartupPanel.SetActive(true);
        this.StartupText.SetText(string.Empty);
        this.spwnmgr.HideLoadingPanel();
        StartupRoutine();
        Speed = HighSpeed;
    }

    private void StartupRoutine()
    {
        // SignalR Setup
        url = @"http://localhost:50000/";

        hubConnection = new HubConnection(url);
        hubConnection.Closed += HubConnection_Closed;
        hubConnection.Error += HubConnection_Error;
        hubConnection.Reconnected += HubConnection_Reconnected;
        hubConnection.Reconnecting += HubConnection_Reconnecting;
        hubProxy = hubConnection.CreateHubProxy("AssetsGroupHub");
        hubProxy.On("ReceiveProductUpdates", x => ReceiveProductData(x));
        hubProxy.On("VerifyConnection", x => VerifyConnection(x));
        hubProxy.On("ReceiveUCBCredit", x => UpdateUCBCredit(x));
        hubProxy.On("ReceiveListOfProducts", x => RecieveProductList(x));
        hubProxy.On("SellResult", x => SellResult(x));

        ConnectToServer();
    }

    private async void ConnectToServer()
    {
        if(hubConnection != null)
        {
            try
            {
                this.StartupText.SetText($"Connecting to SignalR Server listening at {url}");
                await hubConnection.Start();

                this.StartupText.SetText($"Connected succeessfully to SignalR Server at {url}");
                await Task.Delay(1000);
                LoadContent();
            }
            catch (Exception ex)
            {
                this.StartupText.SetText($"Error connecting to SignalR Server - {ex.Message}");
                ServerConnectRetry();
            }
        }
    }

    private void LoadContent()
    {
        hubProxy.Invoke("VerifyServerConnection", "Unity Connect Machine online");
        this.StartupText.SetText("Requesting UCB Credit. . .");
        hubProxy.Invoke("RequestUCBCredit");
        this.StartupText.SetText("Done");
        this.StartupPanel.SetActive(false);
        this.spwnmgr.ShowLoadingPanel(0, "Requesting Assets . . .", "0.0 %", string.Empty);
        hubProxy.Invoke("GetProductsList");
        HandleProductsList();
        hubProxy.Invoke("GetProducts");
        EnableSpawnning = true;
    }

    private async void ServerConnectRetry()
    {
        await Task.Delay(2000);
        this.StartupText.SetText("Retrying connection in 30 seconds . . .");
        await Task.Delay(30000);
        ConnectToServer();
    }

    private void HubConnection_Reconnecting()
    {
        this.StartupPanel.SetActive(true);
        this.StartupText.SetText("Reconnecting to SignalR Server . . .");
        Debug.Log("Reconnecting to SignalR Server . . .");
    }

    private void HubConnection_Reconnected()
    {
        Debug.Log($"Reconnected to SignalR Server {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}");
        this.StartupPanel.SetActive(false);
    }

    private void HubConnection_Error(Exception obj)
    {
        this.StartupPanel.SetActive(true);
        this.StartupText.SetText($"SignalR Client Error - {obj.Message}");
        Debug.Log($"SignalR Client Error - {obj.Message}");
    }

    private void HubConnection_Closed()
    {
        Debug.Log("Connection closed to SignalR Server");
    }

    private void ReceiveProductData(string data)
    {
        List<ProductStuff> psList = JsonConvert.DeserializeObject<List<ProductStuff>>(data);

        Debug.Log($"Got Data, item count is {psList.Count}");

        spwnmgr.LoadNewObjects();
        this.LoadedProducts = psList.ToArray();
    }

    private void VerifyConnection(string data)
    {
        Debug.Log($"Received status from server: {data}");
    }

    private void UpdateUCBCredit(string data)
    {
        this.CurrentUCBCredit = data;

        try
        {
            this.CurrentUCBCreditValue = decimal.Parse(data, System.Globalization.NumberStyles.AllowCurrencySymbol | System.Globalization.NumberStyles.AllowDecimalPoint);
            Debug.Log($"Set Credit To {this.CurrentUCBCreditValue}");
        }
        catch
        {
            Debug.Log("UCB Credit Parse Failed");
        }
    }

    private void RecieveProductList(string data)
    {
        this.productsList = JsonConvert.DeserializeObject<List<string>>(data);
    }

    private async void HandleProductsList()
    {
        this.spwnmgr.ShowLoadingPanel(100, "Requesting Assets . . .", "100.0 %", "Completed!");
        await Task.Delay(1000);
        int index = 0;

        foreach (string item in this.productsList)
        {
            index++;
            this.spwnmgr.ShowLoadingPanel((float)index / this.productsList.Count, "Downloading Assets . . .", string.Format("{0:0.0} %", ((decimal)index / this.productsList.Count) * 100), item);
            await Task.Delay(500);
        }

        await Task.Delay(1000);
        this.spwnmgr.ShowLoadingPanel(0, "Rendering, Please Wait . . .", "0.0 %", string.Empty);
    }

    private void SellResult(string data)
    {
        SaleInProgress = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(EnableSpawnning)
        {
            if (this.LoadedProducts != null && this.LoadedProducts.Length > 0) //this.ProductObjects != null && this.ProductObjects.Length > 0)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (EnableSpiral)
                    {
                        DisableSpin();
                    }
                    else
                    {
                        EnableSpin();
                    }
                }

                SpawnAsset();

                if (EnableSpiral)
                {
                    if (ResetEnvironment)
                    {
                        ResetScene();
                    }

                    if (SpawnedProducts > InitialSpawnCount)
                    {
                        Speed = NormalSpeed;
                    }
                }

                if (Input.GetKey(KeyCode.Insert))
                {
                    SendConnectConfirmation();
                }
            }
        }
        else
        {
            if(Input.GetKeyDown(KeyCode.Insert))
            {
                ResetObjects();
            }
        }

        this.UCBCredit.SetText(CurrentUCBCredit);
        this.ProductCredit.SetText(CurrentUCBCredit);

        if (Input.GetKeyDown(KeyCode.End))
        {
            spwnmgr.VisibleObjects();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void ReturnProductToSpiral()
    {
        foreach(GameObject x in FindObjectsOfType<GameObject>() as GameObject[])
        {
            if(x.GetInstanceID() == this.CurrentProductInstance)
            {
                x.GetComponent<DownwardRotation>().ReturnToTheSpiral();
            }
        }
    }

    private async void SellProductProgress()
    {
        SaleInProgress = true;
        int progress = 0;

        while(SaleInProgress)
        {
            spwnmgr.ShowLoadingPanel((float)progress / 100, "Sending Request to UCB . . .", $"{progress}.0 %", "Processing . . .");
            await Task.Delay(1000);
            progress += 2;

            if(progress >= 100)
            {
                SaleInProgress = false;
            }
        }

        spwnmgr.ShowLoadingPanel(100, "Request Completed", $"100.0 %", string.Empty);
        await Task.Delay(1000);
        spwnmgr.HideLoadingPanel();
        ReturnProductToSpiral();
    }

    private async void GetProducts()
    {
        await hubProxy.Invoke("GetProducts");
    }

    private async void SendConnectConfirmation()
    {
        await hubProxy.Invoke("VerifyServerConnection", $"Cient connected at {DateTime.Now}");
    }

    public async void SellCurrentProduct()
    {
        if(!string.IsNullOrEmpty(CurrentProductID))
        {
            Debug.Log($"Product ID: {CurrentProductID}");
            List<string> prodIds = new List<string>() { CurrentProductID };
            SellProductProgress();
            await hubProxy.Invoke("PurchaseProduct", JsonConvert.SerializeObject(prodIds));
        }
    }

    public void SpawnAsset()
    {
        if (TimeMaster >= MasterOffset)
        {
            spwnmgr.SpawnProduct(MasterOffset);
            SpawnedProducts++;
            objectCount++;
            MasterOffset = SpawnInterval * objectCount;
        }

        if(EnableSpiral)
        {
            TimeMaster += Time.deltaTime * Speed;
        }

        if (TimeMaster < 0)
        {
            TimeMaster = 0;
        }
    }

    public void SetNutritionalImage()
    {
        byte[] imgBytes = this.LoadedProducts.Where(x => x.OBJGUID == this.CurrentDataFileID).FirstOrDefault().NutritionalImage;

        if(imgBytes != null && imgBytes.Length > 0)
        {
            Texture2D tx = new Texture2D(4, 4, TextureFormat.RGBA32, false);
            tx.LoadImage(imgBytes);
            this.NutriImageMaterial.mainTexture = tx;
        }
        else
        {
            Debug.Log("Image bytes were null");
        }
    }

    public void ResetObjects()
    {
        Speed = HighSpeed;
        TimeMaster = 0;
        MasterOffset = 0;
        objectCount = 0;
        SpawnedProducts = 0;
        spwnmgr.ResetObjects();
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void DisableSpin()
    {
        EnableSpiral = false;
    }

    public void EnableSpin()
    {
        EnableSpiral = true;
    }
}

public class ProductStuff
{
    public string ProductName { get; set; }
    public string ProductID { get; set; }
    public string Price { get; set; }
    public byte[] OBJFile { get; set; }
    public string OBJGUID { get; set; }
    public byte[] TextureImage { get; set; }
    public byte[] NutritionalImage { get; set; }
}